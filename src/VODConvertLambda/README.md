## Environment variables
| ENV Name | Description | Example |
| :------- | :---------- | :------ |
| Application | Logical name of the MediaConvert application | VOD |
| DestinationBucket | Destination bucket name for conversion output | lyka-mediaconvert-output-dev |
| MediaConvertEndpoint | MediaConvert account unique API endpoint | https://xxxxxxxxx.mediaconvert.ap-southeast-1.amazonaws.com |
| MediaConvertRole | IAM role ARN to be used for invoking MediaConvert | arn:aws:iam::359582104310:role/MediaConvertServiceRole |
| StatusTableName | DynamoDB table name for marking conversion tasks status | VODConvert |
| TrimVideoExceedSizeInByte | File size threshold in bytes before incurring preview trim function | 9437184 |
| TrimVideoStart | Preview trim start frame | 00:00:00:00 |
| TrimVideoEnd | Preview trim end frame | 00:00:07:00 |
