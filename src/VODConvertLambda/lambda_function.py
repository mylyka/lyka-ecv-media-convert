import glob
import json
import os
import boto3
import random
import logging

from urllib.parse import urlparse, unquote
from botocore.client import ClientError
from datetime import datetime

logger = logging.getLogger()
logger.setLevel(logging.INFO)

S3 = boto3.resource('s3')
dynamodb = boto3.resource('dynamodb')

def put_jobstatus(filename, status, progress=0):

    table = dynamodb.Table(os.environ['StatusTableName'])
    current_ts = int(datetime.timestamp(datetime.utcnow()))

    response = table.put_item(
       Item={
            'assetID': filename,
            'job_status': status,
            'progress': progress,
            'created_at': current_ts,
            'last_updated': current_ts
        }
    )
    return response


def lambda_handler(event, context):
    '''
    Watchfolder handler - this lambda is triggered when video objects are uploaded to the 
    source_s3_bucket/inputs folder.

    It will look for two sets of file inputs:
        source_s3_bucket/inputs/source_s3_key:
            the input video to be converted
        
        source_s3_bucket/jobs/*.json:
            job settings for MediaConvert jobs to be run against the input video. If 
            there are no settings files in the jobs folder, then the Default job will be run 
            from the job.json file in lambda environment. 
    
    Ouput paths stored in outputGroup['OutputGroupSettings']['DashIsoGroupSettings']['Destination']
    are constructed from the name of the job settings files as follows:
        
        s3://<MediaBucket>/<basename(job settings filename)>/<basename(input)>/<Destination value from job settings file>

    '''

    source_s3_bucket = event['Records'][0]['s3']['bucket']['name']
    source_s3_key = event['Records'][0]['s3']['object']['key']
    source_s3_key = unquote(str(source_s3_key).replace('+',' '))
    source_filename = os.path.basename(source_s3_key)
    
    source_s3_uri = 's3://'+ source_s3_bucket + '/' + source_s3_key
    destination_s3_uri = 's3://' + os.environ['DestinationBucket']
    mediaConvertRole = os.environ['MediaConvertRole']
    application = os.environ['Application']
    region = os.environ['AWS_DEFAULT_REGION']
    trim_video_start = os.environ['TrimVideoStart']
    trim_video_end = os.environ['TrimVideoEnd']
    trim_video_size = int(os.environ['TrimVideoExceedSizeInByte'])
    statusCode = 200
    jobs = []
    job = {}
    
    # Use MediaConvert SDK UserMetadata to tag jobs with the assetID 
    # Events from MediaConvert will have the assetID in UserMedata
    jobMetadata = {}
    jobMetadata['assetID'] = source_s3_key
    jobMetadata['application'] = application

    # Output groups setting mapping
    output_group_settings = {
        'FILE_GROUP_SETTINGS': 'FileGroupSettings',
        'HLS_GROUP_SETTINGS': 'HlsGroupSettings',
        'DASH_ISO_GROUP_SETTINGS': 'DashIsoGroupSettings',
        'MS_SMOOTH_GROUP_SETTINGS': 'MsSmoothGroupSettings',
        'CMAF_GROUP_SETTINGS': 'CmafGroupSettings'
    }

    try:    
        
        # Build a list of jobs to run against the input.  Use the settings files in WatchFolder/jobs
        # if any exist.  Otherwise, use the default job.
        
        jobInput = {}
        # Iterates through all the objects in jobs folder of the WatchFolder bucket, doing the pagination for you. Each obj
        # contains a jobSettings JSON
        bucket = S3.Bucket(source_s3_bucket)
        for obj in bucket.objects.filter(Prefix='jobs/'):
            if obj.key != "jobs/":
                jobInput = {}
                jobInput['filename'] = obj.key
                logger.info('jobInput: %s', jobInput['filename'])

                jobInput['settings'] = json.loads(obj.get()['Body'].read())
                logger.info(json.dumps(jobInput['settings'])) 
                
                jobs.append(jobInput)
        
        # Use Default job settings in the lambda zip file in the current working directory
        if not jobs:
            
            with open('standardoutput_preset.json','r') as json_data:
                settings_json_data = json.load(json_data)
                jobs.append({'filename':'standardoutput_preset.json','settings':settings_json_data})
                logger.info('jobInput: %s', 'standardoutput_preset.json') 
                logger.info(json.dumps(settings_json_data))
            
            # if clip is larger than 9MB, trim it 
            # expect previewoutput_preset.json will trim the clip by default
            # or else
            # clip is smaller than 9MB, disable trimming settings
            with open('previewoutput_preset.json','r') as json_data:
                settings_json_data = json.load(json_data)
                if boto3.client('s3').head_object(Bucket=source_s3_bucket, Key=source_s3_key)['ContentLength'] < trim_video_size:
                    del settings_json_data['Inputs'][0]['InputClippings']
                else:
                    settings_json_data['Inputs'][0]['InputClippings'][0]['StartTimecode'] = trim_video_start
                    settings_json_data['Inputs'][0]['InputClippings'][0]['EndTimecode'] = trim_video_end
                    print("The video is TRIMMED")
                jobs.append({'filename':'previewoutput_preset.json','settings':settings_json_data})
                logger.info('jobInput: %s', 'previewoutput_preset.json')
                logger.info(json.dumps(settings_json_data))
                
        # get the account-specific mediaconvert endpoint for this region
        mediaconvert_client = boto3.client('mediaconvert', region_name=region)

        if 'MediaConvertEndpoint' in os.environ:
            endpoint_url = os.environ['MediaConvertEndpoint']
        else:
            endpoints = mediaconvert_client.describe_endpoints()
            endpoint_url = endpoints['Endpoints'][0]['Url']

        # add the account-specific endpoint to the client session 
        mediaconvert_accountspecific_client = boto3.client('mediaconvert', region_name=region, endpoint_url=endpoint_url, verify=False)
        
        for job in jobs:
            jobSettings = job['settings']
            jobFilename = job['filename']

            # Save the name of the settings file in the job userMetadata
            jobMetadata['settings'] = jobFilename

            # Update the job settings with the source video from the S3 event
            jobSettings['Inputs'][0]['FileInput'] = source_s3_uri

            # Update the job settings with the destination paths for converted videos.  We want to replace the
            # destination bucket of the output paths in the job settings, but keep the rest of the
            # path
            base_path = "/".join(source_s3_key.split('/')[:-1])
            destination_s3_key = 's3://' + os.environ['DestinationBucket'] + '/' + base_path

            for outputGroup in jobSettings['OutputGroups']:
                
                logger.info("outputGroup['OutputGroupSettings']['Type'] == %s", outputGroup['OutputGroupSettings']['Type']) 
                
                if outputGroup['OutputGroupSettings']['Type'] in output_group_settings:
                    output_type = outputGroup['OutputGroupSettings']['Type']
                    setting_name = output_group_settings[output_type]

                    templateDestination = outputGroup['OutputGroupSettings'][setting_name]['Destination']
                    templateDestinationKey = urlparse(templateDestination).path
                    logger.info("templateDestinationKey == %s", templateDestinationKey)
                    outputGroup['OutputGroupSettings'][setting_name]['Destination'] = destination_s3_key + templateDestinationKey
                else:
                    logger.error("Exception: Unknown Output Group Type %s", outputGroup['OutputGroupSettings']['Type'])
                    statusCode = 500
            
            logger.info(json.dumps(jobSettings))

            # Convert the video using AWS Elemental MediaConvert
            job = mediaconvert_accountspecific_client.create_job(Role=mediaConvertRole, UserMetadata=jobMetadata, Settings=jobSettings)

            put_jobstatus(source_s3_key, 'CREATED')

    except Exception as e:
        import sys
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logger.error(str(e) + "Exceptions" + str(exc_type) + str(fname) + str(exc_tb.tb_lineno))
        statusCode = 500
        raise

    finally:
        return {
            'statusCode': statusCode,
            'body': json.dumps(job, indent=4, sort_keys=True, default=str),
            'headers': {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'}
        }

