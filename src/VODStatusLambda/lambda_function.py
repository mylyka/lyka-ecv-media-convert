import os
import json
import boto3
import requests

from re import match
from botocore.client import ClientError
from datetime import datetime

dynamodb = boto3.resource('dynamodb')
sqs = boto3.client('sqs')
s3 = boto3.client('s3')

def put_response_send(streamUri):
    try:
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer {}'.format(os.environ['CallbackToken'])
        }

        path = streamUri.split('/')
        filename = path[-1]
        fileId = path[-1].split('.')[0]

        assetId = '/'.join(path[0:-2]) + '/' + fileId + '.mp4'

        data = {
            'assetId': assetId,
            'streamUrl': streamUri
        }

        response = requests.put(os.environ['CallbackEndpoint'] + 'api/v3/posts/UpdateVideoPost',
            headers=headers,
            data=json.dumps(data)
        )

        if response.status_code != 200:
            print(response.content)
            raise Exception('Put opeation failed, response code {}'.format(response.status_code))
    except Exception as err:
        print(err)


def put_response(task):
    for i in range(len(task)):
        try:
            for video in task[i]['playlistFilePaths']:
                if match(".*m3u8$", video):
                    prefixed_name = video[5:].replace(
                                    os.environ['DestinationBucket'] + '/', ''
                                    ).replace('//','/')
                    put_response_send(prefixed_name)
        except:
            pass


def delete_SQS_msg(rid):
    return sqs.delete_message(
        QueueUrl=os.environ['SQSUrl'],
        ReceiptHandle=rid
    )


def update_jobstatus(filename, status, progress=0, ttl=0):
    table = dynamodb.Table(os.environ['StatusTableName'])
    current_ts = int(datetime.timestamp(datetime.utcnow()))

    try:
        response = table.update_item(
            Key={
                'assetID': filename
            },
            UpdateExpression="set job_status=:s, progress=:p, expire=:ttl, last_updated=:u",
            ExpressionAttributeValues={
                ':p': progress,
                ':s': status,
                ':u': current_ts,
                ':completed': 'COMPLETED',
                ':ttl': ttl
            },
            ConditionExpression="((progress <= :p) AND (job_status <> :completed))",
            ReturnValues="UPDATED_NEW"
        )
    except ClientError as e:
        if e.response['Error']['Code'] == "ConditionalCheckFailedException":
            print(e.response['Error']['Message'])
        else:
            raise
    else:
        return response
    

def get_jobstatus(event):
    detail = event['detail']
    status = detail['status']
    
    if status == 'PROGRESSING':
        return 'PROGRESSING', 0
    elif status == 'STATUS_UPDATE':
        progress = detail['jobProgress']['jobPercentComplete']
        
        return 'PROGRESSING', progress
    elif status == 'COMPLETE':

        # PUT the video
        put_response(event['detail']['outputGroupDetails'])
        # rename the JPEG
        #rename_jpeg(event['detail']['outputGroupDetails'])
        
        return 'COMPLETED', 100
    elif status == 'ERROR':
        return 'ERROR', 0
    else:
        raise Exception('Unable to process unsupported status {}'.format(status))


def lambda_handler(event, context):
    messages = event['Records']
    processed = []
    
    for message in messages:
        receiptHandle = message['receiptHandle']
        cw_event = json.loads(message['body'])
        
        try:
            if cw_event['source'] == 'aws.mediaconvert':
                settings = cw_event['detail']['userMetadata']['settings']
                
                if settings == 'standardoutput_preset.json':
                    filename = cw_event['detail']['userMetadata']['assetID']
                    status, progress = get_jobstatus(cw_event)
                    
                    if status == 'COMPLETED':
                        current_ts = int(datetime.timestamp(datetime.utcnow()))
                        update_jobstatus(filename, status, progress, current_ts + 24 * 60 * 60)
                    else:
                        update_jobstatus(filename, status, progress)
        except Exception as e:
            print(e)
        finally:
            processed.append(receiptHandle)
    
    for rid in processed:
        delete_SQS_msg(rid)
    
    return None
