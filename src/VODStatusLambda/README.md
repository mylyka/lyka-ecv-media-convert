## Environment variables
| ENV Name | Description | Example |
| :------- | :---------- | :------ |
| CallbackEndpoint | Callback endpoint for MediaConvert status | https://upload-awsdev.mylykaapps.com/ |
| CallbackToken | Authentication token for callback endpoint | 128 characters token |
| SQSUrl | SQS queue endpoint URL for SQS message deletion | https://sqs.ap-southeast-1.amazonaws.com/359582104310/MediaConvert_JobState |
| StatusTableName | DynamoDB table name for marking conversion tasks status | VODConvert |
