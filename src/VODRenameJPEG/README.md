## Environment variables
| ENV Name | Description | Example |
| :------- | :---------- | :------ |
| DestinationBucket | Destination bucket name for conversion output | lyka-mediaconvert-output-dev |
