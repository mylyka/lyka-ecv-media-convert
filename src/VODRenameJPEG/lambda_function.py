import json
import os
import boto3

def lambda_handler(event, context):
    prefixed_extension_name = event['Records'][0]['s3']['object']['key']
    
    S3 = boto3.resource('s3')
    bucket = S3.Bucket(os.environ['DestinationBucket'])
        
    # copy item with new naming
    copy_source = {
                    'Bucket':os.environ['DestinationBucket'],
                    'Key':prefixed_extension_name
                  }
    obj = bucket.Object(prefixed_extension_name.replace('0000000.jpg', 'jpg'))
    obj.copy(copy_source)

    # delete item with old naming
    obj2 = S3.Object(os.environ['DestinationBucket'], prefixed_extension_name)
    obj2.delete()
