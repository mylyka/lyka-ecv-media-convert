# Lyka Mediaconvert Automation

## Description
### Purpose
- This implementation will automatically convert any uploaded videos to:
  - HLC (video format)
  - MPEG DASH (video format)
  - JPEG (image format, first video frame in first second)
  - MPEG-4 AVC (video format)
    - For this format, video size will be detected to decide whether trimming is required.
    - where depends on environment variables:
      1. `TrimVideoExceedSizeInByte` : if video exceeds this **byte** size, trims it.
      2. `TrimVideoStart` : trim the video from this **time frame**.
      3. `TrimVideoEnd` : trim the video to this **time frame**.
    - where expected format:
      1. **byte** : binary calculation (1024 bytes)
      2. **time frame** : hour:minute:second:frame number, for example:
         - 00:00:01:02 means second frame at first second.
         - 00:01:00:00 means at first minute.
### Log Traceback
- Expect Lambda monitoring is enabled, by default.
- To determine whether video is trimmed, the following output should able to be found in CloudWatch:
  ```
  The video is TRIMMED
  ```

## Services Used
- Lambda
- S3 (as Lambda trigger or input source)
- Mediaconvert (called by Lambda)
- IAM

## Services Configuration
### Lambda
- Lambda Basic Settings
  - Timeout : 2 minutes

</br>

- Trigger : S3
  ```
  Auto-triggered : Enabled
  Event type : ObjectCreated
  Prefix : inputs/
  ```

- Environment Variable

  | Key | Example Value |
  | --- | ----: |
  | Application | VOD |
  | DestinationBucket | BucketName |
  | MediaConvertRole | RoleName <br>(Can be found on *"AWS Example"* in **Reference** section) |
  | TrimVideoEnd | 00:00:03:00 |
  | TrimVideoExceedSizeInByte | 9437184 |
  | TrimVideoStart | 00:00:01:00 |

- Function Code
  ```
  convert.py      # get input video, detect video size, call json files, conditionally remove trimming settings in step2.json
  step1.json      # AWS mediaconvert template, convert video to HLC, MPEG DASH & JPEG
  step2.json      # AWS mediaconvert template, convert video to MPEG-4 ACV, will always trim video by default
  ```

### S3
- Two directories are created in bucket:
  ```
  # Create two empty directories (case sensitive)
  inputs/          # original videos are uploaded into here
  outputs/         # the converted videos will be stored here
  
  # For example, "test.mp4" is uploaded into "inputs/" directory,
  # The overview of filesystem hierarchy will be produced as following:
  MyBucketName:
  ├── inputs/
  │   └── test.mp4
  └── outputs/
      └── test/      
          ├── HLS/
          │   ├── test_HLS_00001.ts
          │   ├── test_HLS.m3u8
          │   └── test.m3u8
          ├── JPEG/
          │   └── test_JPEG.0000000.jpg
          ├── MPEG-4_AVC/
          │   └── test_MPEG-4_AVC.mp4       # either trimmed or not trimmed will have similar prefix name
          └── MPEG_DASH/
              ├── test.mpd
              └── test_MPEG_DASH.mp4

  # hierarchy output may vary based on video frame data
  ```

- Bucket settings
  1. Unblock public access in "Permission" tab.
  1. Enable static web hosting in bucket "Properties" tab, where `index.html` as *"Index document"* name.
  1. Insert CORS Configuration below in bucket "Permission" tab:
      ```
      <?xml version="1.0" encoding="UTF-8"?>
      <CORSConfiguration xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
      <CORSRule>
          <AllowedOrigin>*</AllowedOrigin>
          <AllowedMethod>GET</AllowedMethod>
          <MaxAgeSeconds>3000</MaxAgeSeconds>
          <AllowedHeader>*</AllowedHeader>
      </CORSRule>
      </CORSConfiguration>
      ```

  1. Set bucket policy to public, in "Permission" tab, where below is an S3 generated example:
     ```
     {
       "Version": "2012-10-17",
       "Id": "Policy1597116135491",
       "Statement": [
           {
               "Sid": "Stmt1597116132516",
               "Effect": "Allow",
               "Principal": "*",
               "Action": "s3:GetObject",
               "Resource": "arn:aws:s3:::vod-watchfolder-ecv-zz/*"
           }
        ]
     }
     ```
  1. All mandatory settings (except 4.) can be found on *"AWS Example"* in **Reference** section)
  


## Reference
- [ AWS Example ]

[ AWS Example ]: <https://github.com/aws-samples/aws-media-services-vod-automation.git>
